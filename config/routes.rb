Rails.application.routes.draw do


  get 'rate/:profile_id/:mark' , to: 'rate#rate'
  get 'admin/role/:user_id/:role' , to: 'admin#change_role'
  get 'admin/ban/:user_id/:ban' , to: 'admin#ban'

  devise_for :users, :controllers => { :omniauth_callbacks => "callbacks" }
  get 'tags/show'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'profiles#index'
  resources :admin
  resources :tags,only:[:show]
  resources :profiles do
    resources :photos do
      resources :comments
    end
  end
end


