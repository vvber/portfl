class AddBanToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :ban, :string, default: "unban"
  end
end
