class AddTypeOfSurveyToProfiles < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :type_of_survey, :string
  end
end
