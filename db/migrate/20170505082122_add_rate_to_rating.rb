class AddRateToRating < ActiveRecord::Migration[5.0]
  def change
    add_column :ratings, :rate, :integer
  end
end
