class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|
      t.string :name
      t.integer :year_of_birth
      t.integer :weight
      t.integer :growth
      t.string :sex
      t.integer :rating
      t.timestamps
    end
  end
end
