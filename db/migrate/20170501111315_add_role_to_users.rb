class AddRoleToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :role, :string ,:default => "ROLE_USER"
  end
end
