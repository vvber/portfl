class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :trackable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable,:confirmable, :validatable,
         :omniauthable, :omniauth_providers => [:twitter,:vkontakte]

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create! do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.confirmed_at = Time.now
      user.email = auth.provider + auth.uid.to_s + '@' + auth.provider + '.com'
      user.password = Devise.friendly_token[0,20]
    end
  end
end
