class Profile < ApplicationRecord
  validates :name, presence: true, length: { minimum: 3 }
  validates :weight,:growth, presence: true
  validates :sex,:year_of_birth, presence: true
  has_many :photos, dependent: :destroy
  belongs_to :photo, dependent: :destroy;
  belongs_to :user, dependent: :destroy;
  serialize :type_of_survey
end
