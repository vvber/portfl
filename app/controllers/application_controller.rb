class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_locale

  private

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def default_url_options(options = {})
    { locale: I18n.locale}.merge options
  end

  def require_admin
    if !current_user
      redirect_to new_user_session_path
    else
      unless current_user.role == "ROLE_ADMIN"
        redirect_to "/422"
      end
    end

  end



end
