require 'cloudinary/helper'
require 'algorithmia'
include CloudinaryHelper
class PhotosController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :require_user, only:[:create,:destroy,:update]

  def create
    @profile = Profile.find(params[:profile_id])
    imageCloud = Cloudinary::Uploader.upload( params[:photo][:photo])
    # imageCloud['url']
    input = imageCloud['url']
    puts input
    client = Algorithmia.client('simFN6ip4C7QPE8GJ1B8J1Aqsyr1')
    algo = client.algo('sfw/NudityDetection/1.1.6')
    puts algo.pipe(input).result
    if algo.pipe(input).result['nude'] == 'true'
      puts 'NUDE ALERT'
    else
      puts 'NOT NUDE'
      @photo = @profile.photos.create(:url => imageCloud['url'],:all_tags=>params[:photo][:all_tags])
    end

    redirect_to profile_path(@profile)
  end

  def destroy
    @profile = Profile.find(params[:profile_id])
    @photo = @profile.photos.find(params[:id])
    @photo.destroy
    redirect_to profile_path(@profile)
  end

  def show
    @profile = Profile.find(params[:profile_id])
    @photo = @profile.photos.find(params[:id])
    render layout: false
    #render json: @photo
  end

  def update
    @photo = Photo.find(params[:id])
    @photo.update_attributes(:all_tags=>params[:photo][:all_tags])
    render layout: false
  end

  private
  def require_user
    if current_user
      current_profile = Profile.find_by(user: current_user)
      if current_user.role == "ROLE_ADMIN"
        return
      end
      if params[:profile_id].to_s != current_profile.id.to_s
        redirect_to "/422"
      end
    else
      redirect_to new_user_session_path
    end
  end

end
