require 'cloudinary/helper'
include CloudinaryHelper
class ProfilesController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_profile, only: [:show,:edit,:update,:destroy]
  before_action :require_user, only: [:edit,:update,:destroy]
  def index
    @profiles = Profile.all
    @profiles.each do |profile|
      profile.mark = find_profile_rating( profile ).to_f
    end
    @profiles =  @profiles.sort_by {|obj| obj.mark}.reverse

  end

  def show
  end

  def new
    @profile = Profile.new
  end

  def create
    @profile = Profile.new(profile_params)
    @profile.photo = Photo.new
    @profile.user = current_user
    if @profile.save
      url = photos_url
      if url
        @profile.photo = @profile.photos.create(:url => url)
        @profile.save
      end
      redirect_to @profile
    else
      render :new
    end
  end

  def edit
  end

  def update
    url = photos_url
    if(url)
      @profile.photo = @profile.photos.create(:url => url)
    end
    if !@profile.photo
      @profile.photo = Photo.new
    end
    if @profile.update_attributes(profile_params)
      redirect_to @profile
    else
      render :edit
    end
  end

  def destroy
    if @profile.photo
      @profile.photo.destroy
    end
    @profile.destroy
    redirect_to root_path
  end


  private

  def require_user
    if current_user
      current_profile = Profile.find_by(user: current_user)
      if current_user.role == "ROLE_ADMIN"
        return
      end
      if params[:id].to_s != current_profile.id.to_s
        redirect_to "/422"
      end
    else
      redirect_to new_user_session_path
    end
  end

  def set_profile
    @profile = Profile.find(params[:id])
  end

  def profile_params
    params.require(:profile).permit(:name,:year_of_birth,:growth,:weight,:sex,:photo,:type_of_survey)
    {:name => params[:profile][:name],
     :year_of_birth => Date.new(params[:profile][:'year_of_birth(1i)'].to_i,params[:profile][:'year_of_birth(2i)'].to_i,params[:profile][:'year_of_birth(3i)'].to_i),
     :growth => params[:profile][:growth], :weight => params[:profile][:weight],
     :sex => params[:profile][:sex],:type_of_survey => params[:profile][:type_of_survey]}
  end


  def photos_url
    if params[:profile][:photo]
      imageCloud = Cloudinary::Uploader.upload( params[:profile][:photo]);
      imageCloud['url']
    else
      nil
    end
  end

  def find_profile_rating( profile )
    ratings = Rating.where(:profile =>profile)
    @rate = 0.0
    if ratings
      ratings.each do|rate|
        @rate = @rate + rate.rate
      end
      if(@rate > 0)
        @rate = @rate.to_f/ratings.size
      end
    end
     @rate
  end


end
