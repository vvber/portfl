class RateController < ApplicationController
  before_action :require_user, only: [:rate]
  def rate
    @profile = Profile.find(params[:profile_id])
    rate = Rating.where(:user=>current_user,:profile=>@profile).
        first_or_create(:rate=>params[:mark])
    rate.rate = params[:mark]
    rate.save!
    render layout: false
    #redirect_to profile_path(profile)
  end

  private
  def require_user
    if current_user
        return
    else
      redirect_to new_user_session_path
    end
  end

end
