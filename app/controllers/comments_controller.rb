class CommentsController < ApplicationController
  before_action :require_admin, only:[:destroy]

  def create
    @photo = Photo.find(params[:photo_id])
    current_profile = Profile.find_by(user: current_user)
    if current_profile
      @comment = @photo.comments.create(body:params[:comment][:body],commenter:current_profile.name)
    else
      @comment = @photo.comments.create(body:params[:comment][:body],commenter:'Anonymous')
    end
    render layout: false
   # redirect_to profile_photo_path(@photo.profile,@photo)
  end

  def index
    @photo = Photo.find(params[:photo_id])
    render layout: false
  end

  def destroy
    @photo = Photo.find(params[:photo_id])
    @comment = @photo.comments.find(params[:id])
    @comment.destroy
    render layout: false
    #redirect_to profile_photo_path(@photo.profile,@photo)
  end


end
