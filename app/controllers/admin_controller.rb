class AdminController < ApplicationController
  before_action :require_admin
  def index
    @users = User.all
  end

  def change_role
    user = User.find(params[:user_id])
    user.role = params[:role]
    user.save
    @users = User.all
    render layout: false
  end
  def ban
    user = User.find(params[:user_id])
    user.ban = params[:ban]
    user.save
    @users = User.all
    render layout: false
  end

  def edit
  end

  def update
  end

end
