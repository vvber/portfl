module ProfilesHelper
  def get_avatar(photo)
    if(photo)
      photo.url
    else
      'http://res.cloudinary.com/ddmlz96eh/image/upload/c_limit,h_150,w_100/v1492070468/def_avatar_nfztwr.png'
    end
  end

  def tag_cloud(tags, classes)
    max = tags.sort_by(&:count).last
    tags.each do |tag|
      index = tag.count.to_f/ max.count * (classes.size - 1)
      yield(tag, classes[index.round])
    end
  end
end
