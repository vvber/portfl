module ApplicationHelper

 def create_profile
   if current_user
    profile = Profile.find_by( user: current_user)
     if profile
       false
     else
       true
     end
   else
     false
   end
 end

  def get_my_profile
      profile = Profile.find_by( user: current_user)
  end

 def find_profile_rating( profile )
   ratings = Rating.where(:profile =>profile)
   @rate = 0
   if ratings
     ratings.each do|rate|
       @rate = @rate + rate.rate
     end
     @rate = @rate.to_f/ratings.size
   end
 end

 def find_user_rating( profile )
   rating = Rating.find_by(:profile =>profile,:user=>current_user)
   @rate = 0
   if rating
       @rate = rating.rate
   end
 end

 def require_user
   if current_user
     current_profile = Profile.find_by(user: current_user)
     if current_user.role == "ROLE_ADMIN"
       return true
     end
     if @profile_id.to_s != current_profile.id.to_s
       return false
     end
   else
     return false
   end
   return true
 end

 def require_admin
   if !current_user
     return false
   else
     unless current_user.role == "ROLE_ADMIN"
       return false
     end
   end
   return true
 end

  def ban
    if current_user
      if current_user.ban == "ban"
        return true
      end
    end
    return false
  end

end
